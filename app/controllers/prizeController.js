// Import thư viện mongoose
const mongoose = require("mongoose");

// Import prizes Model
const prizeModel = require('../models/prizeModel');

const getAllprize = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    prizeModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all prize',
         prize: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
   
 }

 //lấy một dữ liệu nước uống bằng id
//-------------------------get a drink by ID 
const getAnprizeById = (request,response) => {
    //b1: thu thập dữ liệu
    const prizeId = request.params.prizeId;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeId)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${prizeId} ko hợp lệ`
       })
    }
    //bb3: gọi model tìm dữ liêu
    prizeModel.findById(prizeId)
       .then((data) => {
          response.status(201).json({
             message: 'successful to find one ',
             productType: data
          })
       })
       .catch((error) => {
          response.status(500).json({
             message: `internal sever ${error}`
          })
       })
 }
 
const createprize = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
   
    
    // B2: Validate dữ liệu
    // Kiểm tra title có hợp lệ hay không
    if( !body.name ) {
        return response.status(400).json({
            status: "Bad Request",
            message: "name  không hợp lệ"
        })
    }



   

    // B3: Gọi Model tạo dữ liệu
    const newprize = {
       
        name: body.name,
        description: body.description,
        
    }

    prizeModel.create(newprize)
     .then((data) => {
          response.status(201).json({
             message: 'Successful to create new newprize',
             dice: data
          })
     })
     .catch((error)=> {
       response.status(500).json({
          message: `internal sever ${error}`
       })
     })
}

const updateprizeById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const prizeId = request.params.prizeId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

   
 
    if(body.name !== undefined && body.name.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "lastname không hợp lệ"
        })
    }
    const vCurrentTime = new Date();
    // B3: Gọi Model tạo dữ liệu
    const updateprize = {
        name: body.name,
        description: body.description,
        
         updatedAt: vCurrentTime
    }

    

    prizeModel.findByIdAndUpdate(prizeId,updateprize)
       .then((data) => {
          response.status(201).json({
             message: 'Successful to update new prize',
             prize: data
          })
       })
       .catch((error) => {
          response.status(500).json({
          message: `internal sever ${error}`
          })
       })
}

const deleteprizeByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const prizeId = request.params.prizeId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "prizeId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    prizeModel.findByIdAndDelete(prizeId)
    .then((data) => {
       response.status(201).json({
          message: 'Successful to delete an prize ',
          productType: data
       })
    })
    .catch((error) => {
       response.status(500).json({
       message: `internal sever ${error}`
       })
    })
}

module.exports = {
    deleteprizeByID,
    updateprizeById,
    createprize,
    getAnprizeById,
    getAllprize
}
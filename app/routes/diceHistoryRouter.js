//khai báo thư viện 
const express = require('express');
const { getAlldiceHistory, getAndiceHistoryById, creatediceHistory, updatediceHistoryById, deletediceHistoryByID } = require('../controllers/diceHistoryController');

//khai báo một biến ROuter chạy trên file index.js
const diceHistoryRouter = express.Router();

diceHistoryRouter.get('/diceHistorys/',getAlldiceHistory);

diceHistoryRouter.get('/diceHistorys/:diceHistoryId',getAndiceHistoryById);

diceHistoryRouter.post('/diceHistorys/',creatediceHistory);
// diceHistoryRouter.post('/diceHistorys/', (req,res) => {
//     const body = req.body
//     res.json({
//         data: body
//     });
// })

diceHistoryRouter.put('/diceHistorys/:diceHistoryId',updatediceHistoryById);

diceHistoryRouter.delete('/diceHistorys/:diceHistoryId',deletediceHistoryByID);

module.exports = {
    diceHistoryRouter
}
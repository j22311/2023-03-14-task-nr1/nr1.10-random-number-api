//khai báo thư viện 
const express = require('express');
const { getAllprize, getAnprizeById, createprize, updateprizeById, deleteprizeByID } = require('../controllers/prizeController');

//khai báo một biến ROuter chạy trên file index.js
const prizeRouter = express.Router();

prizeRouter.get('/prizes/',getAllprize);

prizeRouter.get('/prizes/:prizeId',getAnprizeById);

prizeRouter.post('/prizes/',createprize);
// prizeRouter.post('/prizes/', (req,res) => {
//     const body = req.body
//     res.json({
//         data: body
//     });
// })

prizeRouter.put('/prizes/:prizeId',updateprizeById);

prizeRouter.delete('/prizes/:prizeId',deleteprizeByID);

module.exports = {
    prizeRouter
}
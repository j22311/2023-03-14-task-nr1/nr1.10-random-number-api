//khai báo thư viện 
const express = require('express');
const { getAllVoucher, getAnVoucherById, createVoucher, updateVoucherById, deleteVoucherByID } = require('../controllers/VoucherController');

//khai báo một biến ROuter chạy trên file index.js
const voucherRouter = express.Router();

voucherRouter.get('/vouchers/',getAllVoucher);

voucherRouter.get('/vouchers/:voucherId',getAnVoucherById);

voucherRouter.post('/vouchers/',createVoucher);
// voucherRouter.post('/vouchers/', (req,res) => {
//     const body = req.body
//     res.json({
//         data: body
//     });
// })

voucherRouter.put('/vouchers/:voucherId',updateVoucherById);

voucherRouter.delete('/vouchers/:voucherId',deleteVoucherByID);

module.exports = {
    voucherRouter
}